from mypass_tui.globals import set_api
from mypass_tui.api.mock import MockApi
from mypass_tui.model import VaultEntry, Password
from mypass_tui.ui.app import MyPassApp


def run_ui():
    app = MyPassApp()
    app.run()


if __name__ == "__main__":
    api = MockApi()
    u1, tok = api.register_user("jd", "pass", "John", "Doe", "johndoe@email.com")

    v1 = VaultEntry(
        user_id=u1.meta.id,
        username="fakebook",
        password=Password("fakepass"),
        title="fb",
        website="https://fakebook.com",
        folder="social",
        notes="""
        My random notes to store some info about this vault entry.
        
        In multiple lines ofc.
        """
    )
    api.add_vault(v1)

    v2 = VaultEntry(
        user_id=u1.meta.id,
        username="qmail",
        password=Password("qmailpass"),
        title="qm",
        folder="personal",
        website="https://qmail.com",
        notes="""My random notes to store some info about this vault entry.

        In multiple lines ofc.
        """
    )
    api.add_vault(v2)

    v3 = VaultEntry(
        user_id=u1.meta.id,
        username="something",
        password=Password("passwd123"),
        website="https://qmail.com",
        notes="""My random notes to store some info about this vault entry.

        In multiple lines ofc.
        """
    )
    api.add_vault(v3)

    v4 = VaultEntry(
        user_id=u1.meta.id,
        username="something2",
        password=Password("passwd123"),
        website="https://qmail.com",
        notes="""My random notes to store some info about this vault entry.

        In multiple lines ofc.
        """
    )
    api.add_vault(v4)

    api.register_user("test", "pass123", "Test", "Timmy", "ttimmy@email.com")
    # api.login_user("jd", "pass")

    set_api(api)

    run_ui()
