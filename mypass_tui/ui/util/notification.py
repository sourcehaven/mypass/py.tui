from mypass_tui.exception import ToastMessage
from mypass_tui.globals import settings
from mypass_tui.localization import KEY_FEEDBACK_LEVEL


def show_toast(func):
    def to_severity(sev: str):
        if sev == "error":
            return 3
        if sev == "warning":
            return 2
        if sev == "information":
            return 1

    def wrapper(self, *args, **kwargs):
        try:
            func(self, *args, **kwargs)
        except ToastMessage as e:
            if to_severity(e.severity) >= to_severity(settings[KEY_FEEDBACK_LEVEL]):
                self.notify(message=e.message, title=e.title, severity=e.severity, timeout=e.timeout)
    return wrapper

