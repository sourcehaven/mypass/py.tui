import copy

from mypass_tui.globals import i18n, bindings, settings
from mypass_tui.localization import KEY_BINDINGS, KEY_SETTINGS, get_available_language_codes, get_default_locale, \
    KEY_FEEDBACK_LEVEL
from mypass_tui.model import InputType


def get_binding_input_widgets():
    from mypass_tui.ui.widgets import LabeledInput

    binding_inputs = [
        LabeledInput(
            id=id,
            label_text=i18n[KEY_BINDINGS][id],
            input_initial_value=value,
            required=False,
            input_type=InputType.SINGLE_LINE_TEXT
        )
        for id, value in bindings.items()
    ]
    return binding_inputs


def get_settings_input_widgets():
    from mypass_tui.ui.widgets import LabeledInput

    input_types = InputType.SINGLE_LINE_TEXT, InputType.SWITCH, InputType.SWITCH

    widgets = [
        LabeledInput(
            id=id,
            label_text=i18n[KEY_SETTINGS][id],
            input_initial_value=val,
            input_type=input_types[i],
            required=False,
        ) for i, (id, val) in enumerate(list(settings.items())[:-2])
    ]

    widgets.append(
        LabeledInput(
            id="locale",
            label_text=i18n[KEY_SETTINGS]["locale"],
            input_initial_value=get_available_language_codes(),
            input_default_value=get_default_locale(),
            input_type=InputType.MULTIPLE_CHOICE,
            required=True,
        )
    )

    widgets.append(
        LabeledInput(
            id=KEY_FEEDBACK_LEVEL,
            label_text=i18n[KEY_SETTINGS][KEY_FEEDBACK_LEVEL],
            input_initial_value=["error", "warning", "information"],
            input_default_value=settings[KEY_FEEDBACK_LEVEL],
            input_type=InputType.MULTIPLE_CHOICE,
            required=True,
        )
    )

    return widgets
