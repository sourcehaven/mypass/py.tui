from textual.app import ComposeResult
from textual.binding import Binding
from textual.containers import ScrollableContainer, Container
from textual.screen import Screen
from textual.widgets import Button, Footer, Label

from mypass_tui.exception import ToastMessage
from mypass_tui.globals import i18n, bindings, get_api
from mypass_tui.localization import (
    KEY_BUTTON, KEY_TITLE, KEY_FOOTER,
    KEY_QUIT, KEY_SIGN_IN, KEY_SIGN_UP,
)
from mypass_tui.model import User, UserCreds
from mypass_tui.ui.screens import DialogScreen
from mypass_tui.ui.util.notification import show_toast
from mypass_tui.ui.util.session import exit_app
from mypass_tui.ui.widgets import ButtonPair, PasswordStrength, Password
from mypass_tui.ui.widgets.input_label import (
    scrape_inputs, get_invalid_fields,
    get_labeled_input_widgets_from_base_model, LabeledInput,
)


class SignInScreen(Screen):
    BINDINGS = [
        Binding(bindings[KEY_SIGN_UP], KEY_SIGN_UP, i18n[KEY_FOOTER][KEY_SIGN_UP], show=True),
    ]

    def compose(self) -> ComposeResult:
        yield Container(
            Label(i18n[KEY_TITLE][KEY_SIGN_IN], classes="title"),
            ScrollableContainer(
                *get_labeled_input_widgets_from_base_model(UserCreds, placeholder_key=KEY_SIGN_IN),
                id="input_container",
            ),
            ButtonPair(
                left_text=i18n[KEY_BUTTON][KEY_SIGN_IN],
                right_text=i18n[KEY_BUTTON][KEY_QUIT],
                left_callback=self.on_signin_pressed,
                right_callback=self.on_exit_pressed,
            ),
            classes="screen_container"
        )
        yield Footer()

    def action_sign_up(self):
        def on_sign_up(args):
            from mypass_tui.ui.screens import TokenScreen
            user, token = args[0], args[1]
            self.app.push_screen(TokenScreen(token))

        self.app.push_screen(
            SignUpScreen(),
            callback=on_sign_up,
        )

    @show_toast
    def on_signin_pressed(self, _: Button.Pressed):
        labeled_inputs = self.screen.query(LabeledInput)

        invalid_fields = get_invalid_fields(labeled_inputs)
        if invalid_fields:
            raise ToastMessage(i18n.required(invalid_fields), severity="error")
        else:
            inputs = scrape_inputs(labeled_inputs)
            user = get_api().login_user(**inputs)

            from .main import MainScreen
            self.app.switch_screen(MainScreen())

    def on_exit_pressed(self, _: Button.Pressed):
        exit_app(self.app)


class SignUpScreen(DialogScreen):

    def __init__(self):
        super().__init__(i18n[KEY_TITLE][KEY_SIGN_UP])

    def _compose(self):
        input_widgets = get_labeled_input_widgets_from_base_model(User, placeholder_key=KEY_SIGN_UP)
        password_widget: Password = input_widgets[1].input_widget

        pwstrength_widget = PasswordStrength()
        password_widget.strength_bar = pwstrength_widget
        input_widgets.insert(2, pwstrength_widget)

        yield ScrollableContainer(
            *input_widgets,
            id="input_container",
        )

    @show_toast
    def on_submit_pressed(self, _: Button.Pressed):
        labeled_inputs = self.screen.query(LabeledInput)
        invalid_fields = get_invalid_fields(labeled_inputs)
        if invalid_fields:
            raise ToastMessage(i18n.required(invalid_fields), severity="error")
        else:
            from .token import TokenScreen

            inputs = scrape_inputs(labeled_inputs)
            user, token = get_api().register_user(**inputs)

            self.dismiss((user, token))

    def on_cancel_pressed(self, _: Button.Pressed):
        self.app.pop_screen()
