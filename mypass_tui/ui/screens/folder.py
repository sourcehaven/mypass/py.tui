from textual.app import ComposeResult
from textual.widgets import Static

from mypass_tui.globals import get_api
from mypass_tui.ui.widgets import VaultFolderTree

FOLDER_PAGE_ID = "folder_page"


class FolderPage(Static):
    def compose(self) -> ComposeResult:
        self.folder_widget = VaultFolderTree(get_api().fetch_vaults(get_api().get_user()))
        yield self.folder_widget
