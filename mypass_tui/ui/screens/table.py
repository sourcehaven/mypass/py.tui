from textual.app import ComposeResult
from textual.widgets import Static

from mypass_tui.globals import get_api
from mypass_tui.ui.widgets.vault_table import VaultTable

TABLE_PAGE_ID = "table_page"


class TablePage(Static):
    def compose(self) -> ComposeResult:
        vaults = get_api().fetch_vaults(get_api().get_user())
        self.table = VaultTable(
            id="vault_table",
            zebra_stripes=True,
            vault_entries=vaults,
        )
        yield self.table
