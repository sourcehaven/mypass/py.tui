from textual.binding import Binding
from textual.containers import Container
from textual.screen import Screen
from textual.widgets import Footer

from mypass_tui.globals import i18n, bindings, get_api
from mypass_tui.localization import KEY_FOOTER, KEY_SIGN_OUT
from mypass_tui.model import Password

from mypass_tui.model.vault_entry import TITLE, VaultEntry, USERNAME, PASSWORD, FOLDER, WEBSITE, NOTES
from mypass_tui.ui.screens import (
    TABLE_PAGE_ID,
    FOLDER_PAGE_ID,
    TablePage,
    FolderPage,
    InputScreen,
)

from mypass_tui.ui.util.session import sign_out
from mypass_tui.ui.widgets.input_label import get_labeled_input_widgets_from_base_model


class MainScreen(Screen):
    BINDINGS = [
        Binding(bindings["vault_new"], "new_page", i18n[KEY_FOOTER]["new"]),
        Binding(bindings["vault_table"], TABLE_PAGE_ID, i18n[KEY_FOOTER]["table"]),
        Binding(bindings["vault_folder"], FOLDER_PAGE_ID, i18n[KEY_FOOTER]["folder"]),
        Binding(bindings[KEY_SIGN_OUT], KEY_SIGN_OUT),
    ]

    def __init__(
            self,
            name: str | None = None,
            id: str | None = None,
            classes: str | None = None,
            page_id: str = TABLE_PAGE_ID,
        ):
        super().__init__(name=name, id=id, classes=classes)
        self.page_id = page_id

    def compose(self):
        if self.page_id == TABLE_PAGE_ID:
            page = TablePage()
        elif self.page_id == FOLDER_PAGE_ID:
            page = FolderPage()
        else:
            raise ValueError(f"Unsupported page id: {self.page_id}")

        yield Container(
            page,
            classes="screen_container"
        )

        yield Footer()

    def action_new_page(self):
        def on_create_new(values: dict[str, str | Password]):
            # TODO
            vault = VaultEntry(
                user_id=get_api().get_user().meta.id,
                username=values[USERNAME],
                password=values[PASSWORD],
                title=values[TITLE],
                website=values[WEBSITE],
                folder=values[FOLDER],
                notes=values[NOTES],
            )
            get_api().add_vault(vault)

        self.app.push_screen(
            InputScreen(
                title=i18n[TITLE]["new"],
                inputs=get_labeled_input_widgets_from_base_model(VaultEntry),
            ),
            callback=on_create_new
        )

    def action_table_page(self):
        self.app.switch_screen(MainScreen(page_id=TABLE_PAGE_ID))

    def action_folder_page(self):
        self.app.switch_screen(MainScreen(page_id=FOLDER_PAGE_ID))

    def action_sign_out(self):
        sign_out(self.app)
