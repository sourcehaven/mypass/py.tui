from textual.containers import ScrollableContainer
from textual.widgets import Button
from mypass_tui.exception import ToastMessage
from mypass_tui.globals import i18n
from mypass_tui.localization import KEY_BUTTON

from mypass_tui.ui.screens import DialogScreen
from mypass_tui.ui.util.notification import show_toast
from mypass_tui.ui.widgets import LabeledInput
from mypass_tui.ui.widgets.input_label import get_invalid_fields, scrape_inputs


class InputScreen(DialogScreen):
    def __init__(
        self,
        title: str,
        inputs: list[LabeledInput],
        submit_btn_text: str = i18n[KEY_BUTTON]["submit"],
        cancel_btn_text: str = i18n[KEY_BUTTON]["cancel"],
        name: str | None = None,
        id: str | None = None,
        classes: str | None = None,
        *args,
        **kwargs,
    ):
        self.inputs = inputs
        self.args = args
        self.kwargs = kwargs

        super().__init__(
            title=title,
            submit_btn_text=submit_btn_text,
            cancel_btn_text=cancel_btn_text,
            name=name,
            id=id,
            classes=classes,
        )

    def _compose(self):
        yield ScrollableContainer(
            *self.inputs,
        )

    @show_toast
    def on_submit_pressed(self, _: Button.Pressed):
        invalid_fields = get_invalid_fields(self.inputs)
        if invalid_fields:
            raise ToastMessage(invalid_fields, severity="error")
        else:
            inputs = scrape_inputs(self.inputs)
            self.dismiss(inputs)
