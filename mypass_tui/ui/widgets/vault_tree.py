from typing import Any

from textual.app import App, ComposeResult
from textual.widgets import Tree
from textual.widgets._tree import TreeNode

from mypass_tui.api import BaseApi
from mypass_tui.event import CREATE
from mypass_tui.globals import i18n, get_api
from mypass_tui.localization import KEY_TITLE
from mypass_tui.model import VaultEntry
from mypass_tui.model.vault_entry import update_vault
from mypass_tui.ui.widgets import get_labeled_input_widgets_from_base_model
from mypass_tui.utils.string import split_path

OPEN_FOLDER = "📂 "
CLOSED_FOLDER = "📁 "
FILE = "📄 "


def append_tree_data(root_node: list, path: str, item: Any):
    i = 0
    split_folder = split_path(path)
    depth = len(split_folder)

    def _build_tree(node: list):
        nonlocal i

        for e in node:
            if isinstance(e, tuple) and e[0] == split_folder[i]:
                next_node = e[1]
                break
        else:
            next_node = []
            node.append((split_folder[i], next_node))

        i += 1
        if i == depth:
            next_node.append(item)
        else:
            _build_tree(next_node)

    if depth == 0:
        root_node.append(item)
    else:
        _build_tree(root_node)


def build_tree(tree_data: list, tree_node: TreeNode):
    for branch_data in tree_data:
        if isinstance(branch_data, tuple):
            branch_widget = tree_node.add(CLOSED_FOLDER + branch_data[0])

            build_tree(branch_data[1], branch_widget)
        elif isinstance(branch_data, VaultEntry):
            if branch_data.title:
                leaf_name = FILE + branch_data.title
            else:
                leaf_name = FILE + "<UNTITLED>"
            tree_node.add_leaf(leaf_name, data=branch_data)


class VaultFolderTree(Tree):
    def __init__(self, entries: list[VaultEntry]):
        super().__init__(f".{OPEN_FOLDER}")
        self.entries = entries

        self.tree_structure = []
        self.refresh_tree()

        BaseApi.subscribe_to_vault_events(self)

    def __call__(self, entry: VaultEntry, evt: str):
        if evt == CREATE:
            self.entries.append(entry)
            self.refresh_tree()

    def refresh_tree(self):
        self.tree_structure.clear()

        for entry in self.entries:
            append_tree_data(self.tree_structure, entry.folder, entry)

        self.clear()

        build_tree(self.tree_structure, self.root)
        self.root.expand()

    def on_tree_node_collapsed(self, collapsed: Tree.NodeCollapsed):
        node = collapsed.node
        node.set_label(node.label.plain.replace(OPEN_FOLDER, CLOSED_FOLDER))

    def on_tree_node_expanded(self, expanded: Tree.NodeExpanded):
        node = expanded.node
        node.set_label(node.label.plain.replace(CLOSED_FOLDER, OPEN_FOLDER))

    def on_tree_node_selected(self, selected: Tree.NodeSelected):
        if not selected.node.allow_expand:
            from mypass_tui.ui.screens import InputScreen
            node = selected.node
            entry: VaultEntry = node.data

            def callback(field_dict: dict[str, str]):
                if field_dict:
                    for name, val in field_dict.items():
                        setattr(entry, name, val)

                    get_api().update_vault(entry)
                    update_vault(self.entries, entry)
                    self.refresh_tree()

            self.app.push_screen(
                InputScreen(
                    title=i18n[KEY_TITLE]["edit"],
                    inputs=get_labeled_input_widgets_from_base_model(entry)
                ),
                callback=callback,
            )


class TreeApp(App):
    def compose(self) -> ComposeResult:
        tree: Tree[dict] = Tree("Dune")
        tree.root.expand()
        characters = tree.root.add("Characters", expand=True)
        characters.add_leaf("Paul")
        characters.add_leaf("Jessica")
        characters.add_leaf("Chani")

        characters.add()
        yield tree
