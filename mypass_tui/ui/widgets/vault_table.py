from itertools import cycle
from typing import Callable, Literal

import pyperclip
from textual.binding import Binding
from textual.widgets import DataTable
from textual.widgets._data_table import RowKey

from mypass_tui.api import BaseApi
from mypass_tui.event import CREATE
from mypass_tui.globals import i18n, bindings, get_api
from mypass_tui.localization import KEY_TITLE
from mypass_tui.model import Password, VaultEntry
from mypass_tui.ui.widgets import get_labeled_input_widgets_from_base_model
from mypass_tui.utils.string import to_string

cursors = cycle(["row", "cell"])


class VaultTable(DataTable):

    BINDINGS = [
        Binding(bindings["select"], "select_cursor", "Select", show=False),
        Binding(bindings["copy"], "clipboard_copy", "Copy to clipboard", show=False),
        Binding(bindings["table_mode"], "table_mode", "Switch between cursor modes", show=False),
        Binding(bindings["password_visibility"], "password_visibility", "Show/hide password", show=False),
    ]

    def __init__(
        self,
        vault_entries: list[VaultEntry],
        *,
        show_header: bool = True,
        show_row_labels: bool = True,
        fixed_rows: int = 0,
        fixed_columns: int = 0,
        zebra_stripes: bool = False,
        header_height: int = 1,
        show_cursor: bool = True,
        cursor_foreground_priority: Literal["renderable", "css"] = "css",
        cursor_background_priority: Literal["renderable", "css"] = "renderable",
        cursor_type: Literal["cell", "row", "column", "none"] = "cell",
        name: str | None = None,
        id: str | None = None,
        classes: str | None = None,
        disabled: bool = False,
    ):
        super().__init__(
            show_header=show_header,
            show_row_labels=show_row_labels,
            fixed_rows=fixed_rows,
            fixed_columns=fixed_columns,
            zebra_stripes=zebra_stripes,
            header_height=header_height,
            show_cursor=show_cursor,
            cursor_foreground_priority=cursor_foreground_priority,
            cursor_background_priority=cursor_background_priority,
            cursor_type=cursor_type,
            name=name,
            id=id,
            classes=classes,
            disabled=disabled,
        )

        for label, column_key in zip(i18n.vault_entry_labels(), VaultEntry.FIELD_IDS):
            self.add_column(label=label, key=column_key)

        self.row_counter = 1
        self.vault_entries: dict[str, VaultEntry] = {}

        for entry in vault_entries:
            self.add_entry(entry)

        BaseApi.subscribe_to_vault_events(self)

    def get_row_label(self, row_key: RowKey):
        return self.rows[row_key].label.plain

    def callback(self, vault_id: str):
        def wrapper1(func: Callable[[VaultEntry, dict[str, str]], None]):
            def wrapper2(field_data: dict[str, str]):
                if field_data:
                    vault = self.vault_entries[vault_id]
                    func(vault, field_data)

            return wrapper2

        return wrapper1

    def __call__(self, vault: VaultEntry, evt: str):
        if evt == CREATE:
            self.add_entry(vault)

    @property
    def column_labels(self):
        return tuple(key.label.plain for key in self.columns.values())

    @property
    def row_labels(self):
        return tuple(key.label.plain for key in self.rows.values())

    @property
    def column_keys(self):
        return tuple(key.value for key in self.columns.keys())

    def add_entry(self, vault: VaultEntry):
        key = str(vault.meta.id)
        self.add_row(*vault.row, key=key, label=str(self.row_counter))
        self.vault_entries[key] = vault
        self.row_counter += 1

    def update_entry(self, vault: VaultEntry, updates: dict[str, str]):
        for key, value in updates.items():
            self.update_cell(
                row_key=str(vault.meta.id),
                column_key=key,
                value=value,
                update_width=True,
            )
            setattr(vault, key, value)

        get_api().update_vault(vault)

    def on_data_table_cell_selected(self, selected_cell: DataTable.CellSelected):
        vault_row_id = selected_cell.cell_key.row_key.value
        vault_column_id = selected_cell.cell_key.column_key.value

        inputs = get_labeled_input_widgets_from_base_model(
            self.vault_entries[vault_row_id],
            include_ids=[vault_column_id]
        )

        from mypass_tui.ui.screens import InputScreen
        self.app.push_screen(
            InputScreen(title=i18n[KEY_TITLE]["edit"], inputs=inputs),
            callback=self.callback(vault_row_id)(self.update_entry),
        )

    def on_data_table_row_selected(self, selected_row: DataTable.RowSelected):
        vault_row_id = selected_row.row_key.value

        inputs = get_labeled_input_widgets_from_base_model(self.vault_entries[vault_row_id])

        from mypass_tui.ui.screens import InputScreen
        self.app.push_screen(
            InputScreen(title=i18n[KEY_TITLE]["edit"], inputs=inputs),
            callback=self.callback(vault_row_id)(self.update_entry),
        )

    def action_clipboard_copy(self) -> None:
        def get_value():
            if self.cursor_type == "cell":
                value = self.get_cell_at(self.cursor_coordinate)
                return to_string(value)
            if self.cursor_type == "row":
                return ",".join(to_string(e) for e in self.get_row_at(self.cursor_row))

        def get_notification_message():
            coords = self.cursor_coordinate
            if self.cursor_type == "cell":
                return f"[b]{self.column_labels[coords.column]}[/b] from row [b]{self.row_labels[coords.row]}[/b] has been copied to clipboard"
            if self.cursor_type == "row":
                return f"Values from row [b]{self.row_labels[coords.row]}[/b] has been copied to clipboard"

        self.notify(get_notification_message())
        pyperclip.copy(get_value())

    def action_table_mode(self):
        table = self.screen.query_one(DataTable)
        cursor = next(cursors)
        table.cursor_type = cursor

    def action_password_visibility(self):
        value = self.get_cell_at(self.cursor_coordinate)
        if isinstance(value, Password):
            value = value.toggle()
            self.update_cell_at(self.cursor_coordinate, value, update_width=True)
