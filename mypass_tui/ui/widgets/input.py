from typing import ClassVar, Iterable, Literal, Callable

import pyperclip
from rich.highlighter import Highlighter
from textual.app import ComposeResult
from textual.binding import Binding, BindingType
from textual.containers import Horizontal
from textual.suggester import Suggester
from textual.validation import Validator
from textual.widgets import Input, Static, Label, ProgressBar

import mypass_tui.model.password as mpw
from mypass_tui.exception import ToastMessage
from mypass_tui.globals import i18n, bindings
from mypass_tui.localization import fill_placeholders, KEY_LABEL, KEY_FEEDBACK, select_placeholder, KEY_SUBTITLE
from mypass_tui.ui.util.notification import show_toast


class EpicInput(Input):
    BINDINGS: ClassVar[list[BindingType]] = [
        Binding(bindings["cut"], "cut", show=False),
        Binding(bindings["copy"], "copy", show=False),
        Binding(bindings["paste"], "paste", show=False),
    ]

    DEFAULT_CSS = """
    EpicInput:focus {
        border: tall $accent !important;
    }
    EpicInput:hover {
        border: tall $accent 40%;
    }
    """

    def __init__(
        self,
        id: str | None,
        value: str | None = None,
        placeholder: str = "",
        highlighter: Highlighter | None = None,
        password: bool = False,
        *,
        suggester: Suggester | None = None,
        validators: Validator | Iterable[Validator] | None = None,
        validate_on: Iterable[Literal["blur", "changed", "submitted"]] | None = None,
        name: str | None = None,
        classes: str | None = None,
        disabled: bool = False,
    ):
        super().__init__(
            value=value,
            placeholder=placeholder,
            highlighter=highlighter,
            password=password,
            suggester=suggester,
            validators=validators,
            validate_on=validate_on,
            name=name,
            id=id,
            classes=classes,
            disabled=disabled,
        )

    @show_toast
    def action_cut(self):
        pyperclip.copy(self.value)
        self.value = ""
        raise ToastMessage(
            fill_placeholders(i18n[KEY_FEEDBACK]["info"]["cut"], i18n[KEY_LABEL][self.id]),
            severity="information",
        )

    @show_toast
    def action_copy(self):
        pyperclip.copy(self.value)
        raise ToastMessage(
            fill_placeholders(i18n[KEY_FEEDBACK]["info"]["copy"], i18n[KEY_LABEL][self.id]),
            severity="information",
        )

    def action_paste(self):
        self.value = pyperclip.paste()
        self.cursor_position = len(self.value)


class PasswordStrengthBar(ProgressBar):
    DEFAULT_CSS = """
    PasswordStrengthBar {
        margin-left: 17;
    }
    """

    def __init__(
        self,
        name: str | None = None,
        id: str | None = None,
        classes: str | None = None,
        disabled: bool = False,
    ):
        super().__init__(
            total=len(mpw.PasswordStrength) - 1,
            show_eta=False,
            show_percentage=False,
            name=name,
            id=id,
            classes=classes,
            disabled=disabled,
        )

    def update_progress(self, progress: int, /):
        self.update(progress=progress)


class PasswordStrengthLabel(Label):
    DEFAULT_CSS = """
    PasswordStrengthLabel {
        margin-left: 1;
    }
    """
    pass


class PasswordStrength(Static):
    DEFAULT_CSS = """
    Horizontal {
        height: 1;
    }
    """

    strength_function: Callable[[str], mpw.PasswordStrength] = mpw.default_get_password_strength

    def compose(self) -> ComposeResult:
        self.bar = PasswordStrengthBar()
        self.label = PasswordStrengthLabel()
        horizontal = Horizontal(
            self.bar,
            self.label,
        )
        horizontal.styles.height = 1
        yield horizontal

    def update_strength(self, password: str):
        strength = PasswordStrength.strength_function(password)
        self.bar.update(progress=strength.value)
        self.label.update(str(strength))


class Password(EpicInput):
    BINDINGS = [Binding(bindings["password_visibility"], "show_hide", show=False)]

    def _set_hint(self, show=True):
        show_hide_index = 0 if show else 1
        self.border_subtitle = (
            select_placeholder(
                fill_placeholders(i18n[KEY_SUBTITLE]["show_hide_password"],bindings["password_visibility"])
                , show_hide_index,
            )
        )

    def __init__(
        self,
        value: str | mpw.Password,
        placeholder: str = "",
        strength_bar: PasswordStrength = None,
        *,
        password: bool = None,
        suggester: Suggester | None = None,
        validators: Validator | Iterable[Validator] | None = None,
        name: str | None = None,
        id: str | None = None,
        classes: str | None = None,
        disabled: bool = False,
    ):
        if isinstance(value, mpw.Password):
            password = not value.display
            value = value.data

        if password is None:
            password = True

        super().__init__(
            value=value,
            placeholder=placeholder,
            password=password,
            suggester=suggester,
            validators=validators,
            name=name,
            id=id,
            classes=classes,
            disabled=disabled,
        )
        self.strength_bar = strength_bar
        self._set_hint(show=True)

    @property
    def password_value(self):
        return mpw.Password(self.value, display=not self.password)

    def action_show_hide(self):
        self.password = not self.password
        self._set_hint(self.password)

    def on_input_changed(self, changed_input: Input.Changed):
        if self.strength_bar:
            self.strength_bar.update_strength(changed_input.value)
