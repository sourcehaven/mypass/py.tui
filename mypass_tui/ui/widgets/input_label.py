from typing import Any, Iterable, Type

from textual.app import ComposeResult
from textual.containers import Container
from textual.widget import Widget
from textual.widgets import Input, Label, Static, Switch, TextArea, Select

from mypass_tui.globals import i18n
from mypass_tui.localization import KEY_LABEL
from mypass_tui.model.base import Base
from mypass_tui.model.input_info import InputType
from mypass_tui.ui.widgets import MultilineInput, EpicInput, Password

REQUIRED_TEXT = " [red]*[/red]"


class InputLabel(Label):
    DEFAULT_CSS = """
    InputLabel {
        height: 100%;
        align: center middle;
    }
    """

    def __init__(self, text: str, required: bool = False, length: int = 16):
        super().__init__()
        self.required = required
        self._text = format_field_text(text, length=length, required=required)

    def compose(self):
        yield Label(self._text)

    @property
    def text(self):
        return self._text.replace(REQUIRED_TEXT, "").strip()


class LabeledInput(Static):
    def __init__(
            self,
            id: str,
            label_text: str,
            input_type: InputType,
            input_default_value: Any = None,
            input_initial_value: Any = "",
            placeholder: str = "",
            required: bool = False,
            classes: str | None = None,
            *widgets: Widget,
    ):
        super().__init__(id=id+"_labeled_input", classes=classes)
        self.label_text = label_text
        self.input_initial_value = input_initial_value
        self.input_default_value = input_default_value
        self.input_type = input_type
        self.input_placeholder = placeholder
        self.required = required

        self.label_widget = InputLabel(text=label_text, required=required)
        self.input_widget = self.compose_input_widget()
        self.widgets = widgets

    @property
    def input_id(self):
        return self.id.removesuffix("_labeled_input")

    @property
    def is_valid(self):
        return not self.required or (self.required and bool(self.value))

    def compose_input_widget(self) -> Widget:
        match self.input_type:
            case InputType.SINGLE_LINE_TEXT:
                return EpicInput(id=self.input_id, value=self.input_initial_value, classes="labeled_input", placeholder=self.input_placeholder)
            case InputType.MULTILINE_TEXT:
                return MultilineInput(id=self.input_id, text=self.input_initial_value, classes="labeled_text_area")
            case InputType.PASSWORD:
                return Password(id=self.input_id, value=self.input_initial_value, classes="labeled_input", placeholder=self.input_placeholder)
            case InputType.SWITCH:
                return Switch(id=self.input_id, value=self.input_initial_value, animate=True)
            case InputType.MULTIPLE_CHOICE:
                return Select(
                    id=self.input_id,
                    options=[(val, val) for val in self.input_initial_value], value=self.input_default_value,
                    allow_blank=False, classes="labeled_input"
                )
            case _:
                raise ValueError(f"Invalid input type: {self.input_type}")

    def compose(self) -> ComposeResult:
        yield Container(
            self.label_widget,
            self.input_widget,
            *self.widgets,
            classes="labeled_input_container",
        )

    @property
    def value(self):
        input = self.input_widget

        if isinstance(input, Password):
            return input.password_value
        if isinstance(input, Input):
            return input.value
        if isinstance(input, TextArea):
            return input.text
        if isinstance(input, Switch):
            return input.value
        if isinstance(input, Select):
            return input.value

        assert "Unsupported input widget"

    def has_input(self):
        return bool(self.value)


def get_invalid_fields(labeled_widgets: Iterable[LabeledInput]):
    return [
        labeled_input.label_text.replace(REQUIRED_TEXT, "").strip()
        for labeled_input in labeled_widgets
        if not labeled_input.is_valid
    ]


def format_field_text(text: str, length: int, required=False):
    if required:
        text += REQUIRED_TEXT
        length += len(REQUIRED_TEXT) - 2

    return f"{text:<{length}}"


def scrape_inputs(labeled_widgets: Iterable[LabeledInput]):
    return {lw.id: lw.value for lw in labeled_widgets}


def get_labeled_input_widgets_from_base_model(
        model: Type[Base] | Base,
        include_ids: list = "all",
        placeholder_key: str = ""
):
    if isinstance(model, Base):
        values = model.values
    else:
        values = ("",) * len(model.FIELD_IDS)

    return [
        LabeledInput(
            id=id,
            label_text=i18n[KEY_LABEL][id],
            input_initial_value=value,
            input_type=input_type,
            required=required,
            placeholder=i18n.placeholder(placeholder_key, id) if placeholder_key else "",
        )
        for id, required, input_type, value
        in zip(model.FIELD_IDS, model.REQUIRED, model.INPUT_TYPES, values)
        if include_ids == "all" or id in include_ids
    ]
