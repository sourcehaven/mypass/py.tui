from typing import Literal

from textual.notifications import Notification


class ToastMessage(Exception):

    def __init__(
        self,
        message: str,
        *,
        severity: Literal["information", "warning", "error"],
        title: str = "",
        timeout: float = Notification.timeout
    ):

        if severity == "error":
            self.message = "❌ " + message
        elif severity == "warning":
            self.message = "🚨 " + message
        else:
            self.message = "ℹ️ " + message

        self.title = title
        self.severity = severity
        self.timeout = timeout
