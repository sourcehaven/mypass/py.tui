from typing import Final

from perdict import PerDict

from mypass_tui.api import BaseApi
from mypass_tui.localization import (
    I18N,
    KEY_BINDINGS,
    KEY_SETTINGS,
    KEY_SIGN_IN,
    KEY_SIGN_UP,
    KEY_ABOUT,
    KEY_QUIT,
    KEY_SIGN_OUT,
    KEY_SELECT, KEY_THEME, KEY_TABLE_MODE, KEY_SHOW_HIDE_PASSWORD, KEY_NEXT_TAB, KEY_PREVIOUS_TAB, KEY_POP_SCREEN,
    KEY_COPY, KEY_PASTE, KEY_CUT, KEY_VAULT_NEW, KEY_VAULT_TABLE, KEY_VAULT_FOLDER, KEY_HELP, get_default_locale,
)
from mypass_tui.paths import SETTINGS_PATH, KEY_BINDINGS_PATH

bindings: Final = PerDict(
    KEY_BINDINGS_PATH,
    **{
        KEY_SIGN_IN: "f1",
        KEY_SIGN_UP: "f2",
        KEY_HELP: "f8",
        KEY_ABOUT: "f9",
        KEY_SELECT: "enter",
        KEY_QUIT: "ctrl+q",
        KEY_POP_SCREEN: "escape",
        KEY_SIGN_OUT: "ctrl+l",
        KEY_COPY: "ctrl+c",
        KEY_PASTE: "ctrl+v",
        KEY_CUT: "ctrl+x",
        KEY_VAULT_NEW: "f1",
        KEY_VAULT_TABLE: "f2",
        KEY_VAULT_FOLDER: "f3",
        KEY_PREVIOUS_TAB: "left",
        KEY_NEXT_TAB: "right",
        KEY_SHOW_HIDE_PASSWORD: "ctrl+p",
        KEY_BINDINGS: "ctrl+b",
        KEY_TABLE_MODE: "z",
        KEY_SETTINGS: "ctrl+s",
        KEY_THEME: "ctrl+t",
    }
)

settings: Final = PerDict(
    SETTINGS_PATH,
    password_mask="•",
    confirm_quit=True,
    placeholders=False,
    feedback_level="information",
    locale=get_default_locale()
)

i18n: Final = I18N.from_file(settings["locale"])

_api: BaseApi | None = None


def set_api(__api: BaseApi):
    global _api
    _api = __api


def get_api():
    if _api is None:
        raise Exception("API is not set")
    return _api
