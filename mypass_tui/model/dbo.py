from datetime import datetime

META = "meta"
ID = "id"
CREATED_AT = "created_at"
UPDATED_AT = "updated_at"
DELETED_AT = "deleted_at"


class DboMeta:

    __slots__ = ID, CREATED_AT, UPDATED_AT, DELETED_AT

    def __init__(self, id: int, created_at: datetime, updated_at: datetime = None, deleted_at: datetime = None):
        self.id = id
        self.created_at = created_at
        self.updated_at = updated_at
        self.deleted_at = deleted_at

    @property
    def is_deleted(self):
        return bool(self.deleted_at)
