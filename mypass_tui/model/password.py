import secrets
import string
from collections import UserString
from enum import Enum


class Password(UserString):

    def __init__(self, data: str, display: bool = False):
        super().__init__(data)
        self.display = display

    def toggle(self):
        return Password(self.data, display=not self.display)

    def __str__(self):
        if self.display:
            return self.data
        return len(self.data) * "•"


class PasswordStrength(Enum):
    NOT_SET = 0
    WEAK = 1
    MEDIUM = 2
    STRONG = 3
    VERY_STRONG = 4

    def __str__(self):
        if self.value == 0:
            return ""
        return self.name.capitalize().replace("_", " ")


def default_get_password_strength(password: str):
    if len(password) == 0:
        return PasswordStrength.NOT_SET

    if len(password) < 8:
        return PasswordStrength.WEAK
    has_lowercase = any(char.islower() for char in password)
    has_uppercase = any(char.isupper() for char in password)
    has_digit = any(char.isdigit() for char in password)
    has_special = any(not char.isalnum() for char in password)

    if has_lowercase and has_uppercase and has_digit and has_special:
        return PasswordStrength.VERY_STRONG
    elif has_lowercase and has_uppercase and has_digit:
        return PasswordStrength.STRONG
    elif (has_lowercase or has_uppercase) and has_digit:
        return PasswordStrength.MEDIUM
    else:
        return PasswordStrength.WEAK


def generate_random_password(length: int, *, ascii_letters=True, digits=True, punctuations=True):
    charset = ""

    if ascii_letters:
        charset += string.ascii_letters
    if digits:
        charset += string.digits
    if punctuations:
        charset += string.punctuation

    return ''.join(secrets.choice(charset) for _ in range(length))
