import typing
from dataclasses import dataclass
from enum import Enum


class InputType(Enum):
    SINGLE_LINE_TEXT = "single_line"
    MULTILINE_TEXT = "multiline"
    SWITCH = "switch"
    PASSWORD = "password"
    MULTIPLE_CHOICE = "multichoice"


@dataclass(slots=True)
class InputInfo:
    label_text: str
    type: InputType
    value: typing.Any = ""
    required: bool = False
    default: typing.Any = None
