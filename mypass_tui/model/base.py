import abc

from mypass_tui.model.input_info import InputType


class Base:
    FIELD_IDS: tuple[str, ...]
    REQUIRED: tuple[bool, ...]
    INPUT_TYPES: tuple[InputType, ...]

    @property
    @abc.abstractmethod
    def values(self) -> tuple:
        pass
