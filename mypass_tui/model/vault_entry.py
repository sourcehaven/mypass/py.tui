from typing import Any, Final, Iterable

from .base import Base
from .dbo import DboMeta, ID
from .input_info import InputType
from .password import Password
from mypass_tui.utils.string import to_string

USER_ID: Final = "user_id"
PARENT_ID: Final = "parent_id"
USERNAME: Final = "username"
PASSWORD: Final = "password"
TITLE: Final = "title"
WEBSITE: Final = "website"
FOLDER: Final = "folder"
NOTES: Final = "notes"


class VaultEntry(Base):
    FIELD_IDS = USERNAME, PASSWORD, TITLE, WEBSITE, FOLDER, NOTES
    REQUIRED = True, True, False, False, False, False
    INPUT_TYPES = (
        InputType.SINGLE_LINE_TEXT, InputType.PASSWORD, InputType.SINGLE_LINE_TEXT,
        InputType.SINGLE_LINE_TEXT, InputType.SINGLE_LINE_TEXT, InputType.MULTILINE_TEXT
    )

    __slots__ = (
        USER_ID,
        USERNAME,
        PASSWORD,
        TITLE,
        WEBSITE,
        FOLDER,
        NOTES,
    )

    def __init__(
        self,
        user_id: int,
        username: str = None,
        password: Password = None,
        title: str = None,
        website: str = None,
        folder: str = None,
        notes: str = None,
        parent_id: int = None,
        meta: DboMeta = None,
    ):
        self.user_id = user_id
        self.username = username
        self.password = password
        self.title = title
        self.website = website
        self.folder = folder
        self.notes = notes

        self.parent_id = parent_id
        self.meta = meta

    @classmethod
    def from_values(cls, values: Iterable[Any]):
        instance = cls(*values)
        return instance

    def to_dict(self, password_to_string=True, filter_empty=True) -> dict:
        dct = {field: value for field, value in zip(VaultEntry.FIELD_IDS, self.values)}
        if password_to_string:
            dct[PASSWORD] = to_string(dct[PASSWORD])

        if filter_empty:
            dct = {key: val for key, val in dct.items() if val}
        return dct

    @property
    def values(self):
        return self.username, self.password, self.title, self.website, self.folder, self.notes

    def items(self):
        for k, v in zip(VaultEntry.FIELD_IDS, self.values):
            yield k, v

    @property
    def row(self):
        return (
            self.username or "",
            self.password or "",
            self.title or "",
            self.website or "",
            self.folder or "",
            self.notes or "",
        )

    def __str__(self):
        return (
            f"{self.__class__.__name__}("
            f"{ID}={self.meta.id}, "
            f"{USERNAME}={self.username}, "
            f"{TITLE}={self.title}, "
            f"{WEBSITE}={self.website}, "
            f"{FOLDER}={self.folder}, "
            f"{NOTES}={self.notes}"
        )


def as_dict(entries: list[VaultEntry]):
    return {entry.meta.id: entry for entry in entries}


def as_str_dict(entries: list[VaultEntry]):
    return {str(entry.meta.id): entry for entry in entries}


def update_vault(entries: list[VaultEntry], entry: VaultEntry):
    for i, e in enumerate(entries):
        if e.meta.id == entry.meta.id:
            entries[i] = entry
            return
