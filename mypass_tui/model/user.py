from typing import Final

from .base import Base
from .dbo import DboMeta, META
from .input_info import InputType

USERNAME: Final = "username"
PASSWORD: Final = "password"
EMAIL: Final = "email"
FIRSTNAME: Final = "firstname"
LASTNAME: Final = "lastname"


class UserCreds(Base):

    FIELD_IDS = USERNAME, PASSWORD
    REQUIRED = True, True
    INPUT_TYPES = InputType.SINGLE_LINE_TEXT, InputType.PASSWORD

    __slots__ = USERNAME, PASSWORD

    def __init__(
            self,
            username: str,
            password: str,
            meta: DboMeta = None,
    ):
        self.username = username
        self.password = password
        self.meta = meta

    @property
    def values(self) -> tuple:
        return self.username, self.password


class User(Base):

    FIELD_IDS = USERNAME, PASSWORD, EMAIL, FIRSTNAME, LASTNAME
    REQUIRED = True, True, False, False, False
    INPUT_TYPES = (
        InputType.SINGLE_LINE_TEXT, InputType.PASSWORD, InputType.SINGLE_LINE_TEXT,
        InputType.SINGLE_LINE_TEXT, InputType.SINGLE_LINE_TEXT,
    )

    __slots__ = USERNAME, PASSWORD, EMAIL, FIRSTNAME, LASTNAME, META

    def __init__(
            self,
            username: str,
            password: str,
            email: str = None,
            firstname: str = None,
            lastname: str = None,
            meta: DboMeta = None,
    ):
        self.username = username
        self.password = password
        self.email = email
        self.firstname = firstname
        self.lastname = lastname
        self.meta = meta

    @property
    def values(self) -> tuple:
        return self.username, self.password, self.email, self.firstname, self.lastname
