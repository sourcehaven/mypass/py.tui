import abc

from mypass_tui.event import CREATE, UPDATE, DELETE
from mypass_tui.model import User, VaultEntry


class BaseApi(abc.ABC):
    _user_event_subs = []
    _vault_event_subs = []

    @classmethod
    def subscribe_to_user_events(cls, obj):
        cls._user_event_subs.append(obj)

    @classmethod
    def subscribe_to_vault_events(cls, obj):
        cls._vault_event_subs.append(obj)

    def _notify_on_vault_event(self, vault: VaultEntry, event: str):
        for sub in BaseApi._vault_event_subs:
            sub(vault, event)

    def _notify_on_user_event(self, user: User, event: str):
        for sub in BaseApi._user_event_subs:
            sub(user, event)

    @classmethod
    def get_user(cls) -> User | None:
        pass

    @abc.abstractmethod
    def _register_user(
            self,
            username: str,
            password: str,
            firstname: str = None,
            lastname: str = None,
            email: str = None
    ) -> tuple[User, str]:
        pass

    def register_user(
            self,
            username: str,
            password: str,
            firstname: str = None,
            lastname: str = None,
            email: str = None
    ) -> tuple[User, str]:
        ret = self._register_user(username=username, password=password, firstname=firstname, lastname=lastname, email=email)
        self._notify_on_user_event(ret[0], CREATE)
        return ret

    @abc.abstractmethod
    def login_user(self, username: str, password: str) -> User:
        pass

    @abc.abstractmethod
    def logout_user(self, user: User):
        pass

    @abc.abstractmethod
    def fetch_vault(self, id: int) -> VaultEntry:
        pass

    @abc.abstractmethod
    def fetch_vaults(self, user: User) -> list[VaultEntry]:
        pass

    @abc.abstractmethod
    def _add_vault(self, vault: VaultEntry):
        pass

    def add_vault(self, vault: VaultEntry):
        ret = self._add_vault(vault)
        self._notify_on_vault_event(vault, CREATE)
        return ret

    @abc.abstractmethod
    def _delete_vault(self, vault: VaultEntry):
        pass

    def delete_vault(self, vault: VaultEntry):
        ret = self._delete_vault(vault)
        self._notify_on_vault_event(vault, DELETE)
        return ret

    @abc.abstractmethod
    def _update_vault(self, vault: VaultEntry):
        pass

    def update_vault(self, vault: VaultEntry):
        ret = self._update_vault(vault)
        self._notify_on_vault_event(vault, UPDATE)
        return ret
