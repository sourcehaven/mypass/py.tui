from datetime import datetime

from .base import BaseApi
from ..exception import ToastMessage
from ..model import VaultEntry, User
from ..model.dbo import DboMeta


def now():
    return datetime.now()


class MockApi(BaseApi):

    _logged_in_user: User | None = None

    _users: list[User] = []
    _entries: list[VaultEntry] = []

    _user_counter = 0
    _vault_counter = 0

    @classmethod
    def get_user(cls) -> User | None:
        if cls._logged_in_user:
            return cls._logged_in_user

    def _register_user(
            self,
            username: str,
            password: str,
            firstname: str = None,
            lastname: str = None,
            email: str = None
    ) -> tuple[User, str]:
        from uuid import uuid4

        self._user_counter += 1
        user = User(
            username=username,
            password=password,
            firstname=firstname,
            lastname=lastname,
            email=email,
            meta=DboMeta(id=self._user_counter, created_at=now()),
        )

        self._users.append(user)
        return user, str(uuid4())

    def login_user(self, username: str, password: str) -> User:
        for user in self._users:
            if user.username == username and user.password == password:
                MockApi._logged_in_user = user
                return user
        raise ToastMessage("User does not exists with this credentials")

    def logout_user(self, user):
        if MockApi._logged_in_user and MockApi._logged_in_user.meta.id == user.meta.id:
            MockApi._logged_in_user = None

    def fetch_vault(self, id: int) -> VaultEntry:
        for entry in self._entries:
            if entry.meta.id == id:
                return entry
        raise ToastMessage("Unable to find vault entry with this id")

    def fetch_vaults(self, user: User) -> list[VaultEntry]:
        def user_entries():
            for entry in self._entries:
                if entry.user_id == user.meta.id and not entry.meta.is_deleted:
                    yield entry
        return list(user_entries())

    def _add_vault(self, vault: VaultEntry):
        self._vault_counter += 1
        vault.meta = DboMeta(id=self._vault_counter, created_at=now())
        self._entries.append(vault)

    def _delete_vault(self, vault: VaultEntry):
        for entry in self._entries:
            if entry.meta.id == vault.meta.id:
                if entry.meta.is_deleted:
                    raise ToastMessage("Unable to update already deleted vault entry")
                entry.meta.deleted_at = now()
                return
        raise ToastMessage("Vault entry not found")

    def _update_vault(self, vault: VaultEntry):
        for i, entry in enumerate(self._entries):
            if entry.meta.id == vault.meta.id:
                if entry.meta.is_deleted:
                    raise ToastMessage("Unable to update deleted vault entry")
                self._entries[i].meta.updated_at = now()
                self._entries[i] = vault
                return
        raise ToastMessage("Vault entry not found")
